package tech.gen.onboarding

import android.arch.lifecycle.ViewModelProvider
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_first.*
import tech.gen.onboarding.injection.observe
import tech.gen.onboarding.injection.withViewModel
import javax.inject.Inject

class FirstFragment : Fragment() {
    companion object {
        val TAG = "FirstFragment"
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: OnboardingViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_first, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        getAppInjector().inject(this)
        activity?.withViewModel<OnboardingViewModel>(viewModelFactory) {
            viewModel = this
            observe(inputLiveData, ::updateUi)
        }
        next_button.setOnClickListener {
            val position = when (goal.checkedRadioButtonId) {
                R.id.loose_weight -> 0
                R.id.stay_fit -> 1
                R.id.heart -> 2
                R.id.endurance -> 3
                else -> throw Exception()
            }
            viewModel.onGoalSelected(position)
            viewModel.onNextClick()
        }
    }

    private fun updateUi(userInputData: UserInputData?) {
        userInputData?.let {
            goal.check(goal.getChildAt(userInputData.goal.ordinal).id)
        }
    }
}