package tech.gen.onboarding

import android.arch.lifecycle.ViewModelProvider
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_onboarding.*
import tech.gen.onboarding.injection.observe
import tech.gen.onboarding.injection.withViewModel
import javax.inject.Inject

class OnboardingActivity : AppCompatActivity() {


    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var onboardingViewModel: OnboardingViewModel

    @Inject
    lateinit var navigator: OnboardingNavigator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_onboarding)
        getAppInjector().inject(this)
        withViewModel<OnboardingViewModel>(viewModelFactory) {
            onboardingViewModel = this
            observe(navigationState, ::updateUi)
            onNextClick()
        }

        back_button.setOnClickListener { onBackPressed() }
    }

    private fun updateUi(navigationState: NavigationState?) {
        navigator.showState(this, navigationState)
    }

    override fun onBackPressed() {
        onboardingViewModel.onBackClick()
    }

    fun setCurrentScreen(i: Int) {
        current_screen.text = "screen $i"
    }

}
