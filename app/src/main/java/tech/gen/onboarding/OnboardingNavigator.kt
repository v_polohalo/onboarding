package tech.gen.onboarding

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v4.app.FragmentManager
import javax.inject.Inject

class OnboardingNavigator @Inject constructor() {
    //can't just use single activity, because of subscription screen


    fun showState(activity: OnboardingActivity, state: NavigationState?) {
        state?.let {
            when (it) {
                is NavigationState.FirstTab -> {
                    showFirstTab(activity)
                    activity.setCurrentScreen(1)
                }
                is NavigationState.SecondTab -> {
                    showSecondTab(activity)
                    activity.setCurrentScreen(2)
                }
                is NavigationState.ThirdTab -> {
                    showThirdTab(activity)
                    activity.setCurrentScreen(3)
                }
                is NavigationState.Finish -> {
                    closeOnboarding(activity)
                }
            }
        }
    }

    private fun showFirstTab(activity: FragmentActivity) {
        if (!popIfExists(activity, FirstFragment.TAG))
            showFragment(FirstFragment(), activity.supportFragmentManager, FirstFragment.TAG)
    }

    private fun showSecondTab(activity: FragmentActivity) {
        if (!popIfExists(activity, SecondFragment.TAG))
            showFragment(SecondFragment(), activity.supportFragmentManager, SecondFragment.TAG)
    }

    private fun showThirdTab(activity: FragmentActivity) {
        if (!popIfExists(activity, ThirdFragment.TAG))
            showFragment(ThirdFragment(), activity.supportFragmentManager, ThirdFragment.TAG)
    }

    private fun closeOnboarding(activity: FragmentActivity) {
        activity.finish()
    }

    private fun popIfExists(activity: FragmentActivity, tag: String): Boolean {
        val manager = activity.supportFragmentManager
        return manager.popBackStackImmediate(tag, 0)
    }

    private fun showFragment(fragment: Fragment, supportFragmentManager: FragmentManager, tag: String) {
        supportFragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.slide_from_right,
                        R.anim.slide_to_left,
                        R.anim.slide_from_left,
                        R.anim.slide_to_right)
                .replace(R.id.container, fragment, tag)
                .addToBackStack(tag)
                .commit()
    }
}

sealed class NavigationState {
    object FirstTab : NavigationState()
    object SecondTab : NavigationState()
    object ThirdTab : NavigationState()
    object Finish : NavigationState()
}

class NavigationStateMapper @Inject constructor() {

    fun getNextState(currentState: NavigationState?): NavigationState {
        return when (currentState) {
            null -> {
                NavigationState.FirstTab
            }
            is NavigationState.FirstTab -> {
                NavigationState.SecondTab
            }
            is NavigationState.SecondTab -> {
                NavigationState.ThirdTab
            }
            is NavigationState.ThirdTab -> {
                NavigationState.Finish
            }
            else -> throw IllegalStateException("onboarding can't be in $currentState state")
        }
    }

    fun getPreviousState(currentState: NavigationState?): NavigationState? {
        return when (currentState) {
            is NavigationState.FirstTab -> {
                NavigationState.Finish
            }
            is NavigationState.SecondTab -> {
                NavigationState.FirstTab
            }
            is NavigationState.ThirdTab -> {
                NavigationState.SecondTab
            }
            else -> throw IllegalStateException("onboarding can't be in $currentState state")
        }
    }
}
