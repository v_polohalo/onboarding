package tech.gen.onboarding

import android.arch.lifecycle.ViewModelProvider
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_second.*
import tech.gen.onboarding.injection.observe
import tech.gen.onboarding.injection.withViewModel
import javax.inject.Inject

class SecondFragment : Fragment() {
    companion object {
        val TAG = "SecondFragment"
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: OnboardingViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_second, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        getAppInjector().inject(this)
        activity?.withViewModel<OnboardingViewModel>(viewModelFactory) {
            viewModel = this
            observe(inputLiveData, ::updateUi)
        }


        next_button.setOnClickListener {
            val position = when (frequency.checkedRadioButtonId) {
                R.id.not_much -> 0
                R.id.normal -> 1
                R.id.a_lot -> 2
                else -> throw Exception()
            }
            viewModel.onWalkFrequencySelected(position)
            viewModel.onNextClick()
        }
    }

    private fun updateUi(userInputData: UserInputData?) {
        userInputData?.let {
            frequency.check(frequency.getChildAt(userInputData.walkFrequency.ordinal).id)
        }
    }
}
