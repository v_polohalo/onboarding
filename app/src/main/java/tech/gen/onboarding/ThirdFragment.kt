package tech.gen.onboarding

import android.arch.lifecycle.ViewModelProvider
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_first.*
import tech.gen.onboarding.injection.observe
import tech.gen.onboarding.injection.withViewModel
import javax.inject.Inject

class ThirdFragment : Fragment() {
    companion object {
        val TAG = "ThirdFragment"
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: OnboardingViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_third, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        getAppInjector().inject(this)
        activity?.withViewModel<OnboardingViewModel>(viewModelFactory) {
            viewModel = this
            observe(inputLiveData, ::updateUi)
        }


        next_button.setOnClickListener {
            viewModel.onNextClick()
        }
    }

    private fun updateUi(userInputData: UserInputData?) {
        userInputData?.let {
            //todo update ui
        }
    }
}