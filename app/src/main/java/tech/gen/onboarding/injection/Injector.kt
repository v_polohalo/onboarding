package tech.gen.onboarding.injection

import dagger.Component
import tech.gen.onboarding.FirstFragment
import tech.gen.onboarding.OnboardingActivity
import tech.gen.onboarding.SecondFragment
import tech.gen.onboarding.ThirdFragment
import javax.inject.Singleton

@Singleton
@Component(modules = [ViewModelModule::class,
    ContextModule::class])
interface Injector {
    fun inject(onboardingActivity: OnboardingActivity)
    fun inject(firstFragment: FirstFragment)
    fun inject(secondFragment: SecondFragment)
    fun inject(thirdFragment: ThirdFragment)

}