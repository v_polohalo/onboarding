package tech.gen.onboarding

import android.app.Activity
import android.app.Application
import android.support.v4.app.Fragment
import tech.gen.onboarding.injection.ContextModule
import tech.gen.onboarding.injection.DaggerInjector
import tech.gen.onboarding.injection.Injector

class App : Application(){
    private lateinit var injector: Injector


    override fun onCreate() {
        super.onCreate()
        initializeInjector()

    }

    private fun initializeInjector() {
        injector = DaggerInjector.builder()
                .contextModule(ContextModule(this))
                .build()
    }

    fun getApplicationComponent(): Injector {
        return injector
    }

}

fun Fragment.getAppInjector(): Injector {
    return (activity?.application as App).getApplicationComponent()
}

fun Activity.getAppInjector(): Injector {
    return (application as App).getApplicationComponent()
}