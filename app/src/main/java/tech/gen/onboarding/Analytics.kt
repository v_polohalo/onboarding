package tech.gen.onboarding

import android.util.Log
import javax.inject.Inject

class Analytics @Inject constructor(){
    fun sendEvent(data: UserInputData?) {
        Log.e("analytics", data.toString())
    }
}