package tech.gen.onboarding

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import javax.inject.Inject

class OnboardingViewModel @Inject constructor(private val repository: DataRepository,
                                              private val analytics: Analytics,
                                              private val navigationStateMapper: NavigationStateMapper) : ViewModel() {

    val inputLiveData: MutableLiveData<UserInputData> = MutableLiveData()

    val navigationState: MutableLiveData<NavigationState> = MutableLiveData()

    init {
        inputLiveData.value = currentInputData
        repository.loadData()
    }

    private val currentInputData: UserInputData
        get() = inputLiveData.value ?: UserInputData()

    fun onGoalSelected(position: Int) {
        inputLiveData.value = currentInputData.copy(goal = mapToGoal(position))
        analytics.sendEvent(inputLiveData.value)
    }

    fun onWalkFrequencySelected(position: Int) {
        inputLiveData.value = currentInputData.copy(walkFrequency = mapToFrequency(position))
        analytics.sendEvent(inputLiveData.value)
    }

    fun onCurrentWeightSelected(value: Int) {
        inputLiveData.value = currentInputData.copy(weight = value)
        analytics.sendEvent(inputLiveData.value)
    }

    fun onTargetWeightSelected(value: Int) {
        inputLiveData.value = currentInputData.copy(targetWeight = value)
        analytics.sendEvent(inputLiveData.value)
    }

    fun onNextClick() {
        navigationState.value = navigationStateMapper.getNextState(navigationState.value)
        analytics.sendEvent(inputLiveData.value)
    }

    private fun mapToGoal(position: Int): Goal {
        return when (position) {
            0 -> Goal.LOOSE_WEIGHT
            1 -> Goal.STAY_FIT
            2 -> Goal.HEART
            3 -> Goal.ENDURANCE
            else -> throw IllegalStateException("no $position position")
        }
    }

    private fun mapToFrequency(position: Int): WalkFrequency {
        return when (position) {
            0 -> WalkFrequency.NOT_MUCH
            1 -> WalkFrequency.NORMAL
            2 -> WalkFrequency.A_LOT
            else -> throw IllegalStateException("no $position walkFrequency")
        }
    }

    fun onBackClick() {
        navigationState.value = navigationStateMapper.getPreviousState(navigationState.value)
    }
}

data class UserInputData(val goal: Goal = Goal.STAY_FIT,
                         val walkFrequency: WalkFrequency = WalkFrequency.NORMAL,
                         val weight: Int = 0,
                         val targetWeight: Int = 0)

enum class Goal {
    LOOSE_WEIGHT,
    STAY_FIT,
    HEART,
    ENDURANCE,
}

enum class WalkFrequency {
    NOT_MUCH,
    NORMAL,
    A_LOT
}